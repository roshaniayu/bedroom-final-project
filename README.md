# Bedroom Project

Repositori _git_ ini berisi kode sumber untuk program Bedroom yang dibuat oleh **Kilimpik Grifkim**. Program ini dibuat untuk memenuhi pengerjaan **Proyek Akhir** mata kuliah Grafika Komputer Semester Genap 2019/2020, Fakultas Ilmu Komputer, Universitas Indonesia.

Anggota Kilimpik Grifkim:

1. Gagah Pangeran Rosfatiputra - 1706039566
2. Giovan Isa Musthofa - 1706040126
3. Muhammad Nadhirsyah Indra -1706039383
4. Roshani Ayu Pranasti - 1706026052

![Bedroom Project](https://files.catbox.moe/najzwv.png)
![Bedroom Project](https://files.catbox.moe/4mxpyn.png)

## Cara Menjalankan Program

Catatan: Chrome dan Mozilla (mungkin juga _browser_ lainnya) tidak dapat langsung menjalankan _script_ dari folder tersebut karena alasan keamanan. Solusinya adalah menjalankannya melalui **_webserver/localhost_** atau **menonaktifkan _web security_**.

Menjalankan melalui _webserver/localhost_:

1. Menggunakan python versi 3 atau lebih dengan menuliskan perintah `python -m http.server` atau `python3 -m http.server` pada terminal.
2. Membuka _browser_ dan program dapat diaskes via `http://localhost:8000/`.

## Proses Pembentukan Objek

1. Membuat seluruh objek dan _scene_ pada aplikasi **Blender**.
2. Setiap objek memiliki **_Mesh_** nya masing-masing.

## Proses Rendering Objek dan Scene

1. File Blender yang sebelumnya sudah dibuat lalu di-_export_ ke dalam _file_ berbentuk **Wavefront (.obj)**.
2. Men-_convert_ _file_ Wavefront (.obj) tersebut ke dalam _file_ **.json** menggunakan _library_ `assimp2json`.
3. Memasukkan _file_ .json ke dalam _project_.
4. Me-_render_ setiap _Mesh_ yang ada pada file .json untuk membuat objek dan _scene_. Contoh implementasi dapat dilihat pada file BedroomScene.js.

## Fasilitas WebGL yang Digunakan

- assimp2json: https://github.com/acgessler/assimp2json

## Objek Model Hirarki

**Kursi**
```
ChairStandMesh
└── ChairMesh
```

**Pintu**
```
DoorMesh
└── DoorKnobMesh
```

**Bola**
```
ShelfMesh
└── ShelfBallMesh
```

## Sumber Cahaya

![Secret-angle](https://files.catbox.moe/vr440b.png)

Berikut adalah 3 sumber cahaya yang terdapat pada _scene_.

1. Sumber cahaya **_directional light_** berasal dari sisi dinding yang tidak memiliki objek yang menempel.
2. Sumber cahaya **_point light_** berasal dari arah objek lampu. Sumber cahaya yang menghasilkan bayangan. Bergerak dalam mode demo. Bisa digerakkan secara interaktif. Bisa dimatikan.
3. Sumber cahaya **_spot light_** berasal dari balik monitor untuk meniru cahaya layar komputer yang sedang menyala.

Pada implementasi ini tiap sumber cahaya memiliki intensitas cahayanya masing, dan intensitas cahaya akhir yang diterapkan pada permukaan benda adalah intensitas yang paling terang.

## Texturing

_Texturing_ kami baru hanya sampai memuat _texture_ dan membuat sampler2D di _shader_. Penerapan _texturing_ lebih lanjut belum bisa kami selesaikan tepat waktu karena masih terdapat kendala dengan _texture_ koordinat dan konflik dengan penerapan fitur nyala/mati _point lighting_.

## Log Pekerjaan

Semua anggota kelompok tidak hanya fokus kepada hal yang mereka kerjakan saja, tetapi juga fokus ke hal lain sebagai ajang untuk saling membantu jika ada yang kesusahan.

Gagah Pangeran Rosfatiputra (1706039566)

- Membuat seluruh objek serta _scene_ pada Blender.

Giovan Isa Musthofa (1706040126)

- Membuat titik pandang/kamera umum dan dari objek kursi.
- Mengimplementasikan 3 sumber cahaya, yaitu _directional light_, _point light_, dan _spot light_.
- Menerapkan _texturing_ pada objek maupun _scene_.

Muhammad Nadhirsyah Indra (1706039383)

- Membuat objek model hirarki.
- Membuat animasi pada objek yang hirarki.
- Membuat objek dapat digerakkkan oleh _user_.

Roshani Ayu Pranasti (1706026052)

- Me-_render_ seluruh objek serta _scene_ dari Blender ke dalam WebGL.
- Mengimplementasikan opsi dua mode objek, yaitu _wireframe_ dan _shading_.
- Mengimplementasikan opsi dua mode display, yaitu demo dan interaktif.
- Mengimplementasikan _shadows_.
- Menambahkan mode lampu mati.
- Membuat seluruh tampilan HTML dan CSS.
- Membuat dokumentasi pada README.md dan pdf.

## Referensi

- https://youtu.be/fNK1E5DdYxk
- https://webglfundamentals.org/
