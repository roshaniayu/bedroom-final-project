precision mediump float;

uniform vec3 pointLightPosition;
uniform vec4 meshColor;

uniform sampler2D sampler;
uniform samplerCube lightShadowMap;
uniform vec2 shadowClipNearFar;

uniform float bias;

varying vec3 fPos;
varying vec3 fNorm;
varying vec2 fragTexCoord;

void main()
{
	float spotLightLimit = 0.9;
	vec3 sLightDirection = vec3(-1.0, 0.0, 0.0);
	vec3 spotLightPosition = vec3(6.0, 2.2, 1.7);
	vec3 toLightNormal = normalize(pointLightPosition - fPos);
	vec3 toSLigthNormal = normalize(spotLightPosition - fPos);

	float dotSLightFromDirection = dot(toSLigthNormal, -sLightDirection);

	float fromLightToFrag =
		(length(fPos - pointLightPosition) - shadowClipNearFar.x)
		/
		(shadowClipNearFar.y - shadowClipNearFar.x);

	float shadowMapValue = textureCube(lightShadowMap, -toLightNormal).r;

	float sLightIntensity = 0.1;
	if (dotSLightFromDirection >= spotLightLimit) {
		sLightIntensity += max(dot(fNorm, toSLigthNormal), 0.0);
	}

	float dLightIntensity = 0.1 + 0.5 * dot(fNorm, vec3(-0.2, -1.0, 0.6));

	float plightIntensity = 0.1;
	if ((shadowMapValue + bias) >= fromLightToFrag) {
		plightIntensity += 0.8 * max(dot(fNorm, toLightNormal), 0.0);
	}

	float light = max(plightIntensity, dLightIntensity);
	light = max(light, sLightIntensity);

	// Generate texel
	highp vec4 texelColor = texture2D(sampler, fragTexCoord);

	// Pass texel + real color + lightning
	gl_FragColor = vec4(meshColor.rgb * light, meshColor.a);
	//gl_FragColor = vec4(texelColor.rgb * light, meshColor.a);
	//gl_FragColor = vec4((texelColor.rgb + meshColor.rgb) * light, texelColor.a + meshColor.a);
}
