

'use strict';

var BedroomScene = function (gl) {
	this.gl = gl;
	this.objectMode = "shading";
	this.demoFlag = true;
	this.lightOn = true;

	this.chairAngle = 0;
	this.ballTranslate = -2.1;
	this.angleKnob = -135;
	this.doorTranslate = -3.65;
	this.shelfTranslate=0;
};

BedroomScene.prototype.Load = function (cb) {
	console.log('Loading bedroom scene');

	var me = this;

	async.parallel({
		Models: function (callback) {
			async.map({
				RoomModel: 'Bedroom.json'
			}, LoadJSONResource, callback);
		},
		ShaderCode: function (callback) {
			async.map({
				'Shadow_VSText': 'shaders/Shadow.vs.glsl',
				'Shadow_FSText': 'shaders/Shadow.fs.glsl',
				'ShadowMapGen_VSText': 'shaders/ShadowMapGen.vs.glsl',
				'ShadowMapGen_FSText': 'shaders/ShadowMapGen.fs.glsl'
			}, LoadTextResource, callback);
		}
	}, function (loadErrors, loadResults) {
		if (loadErrors) {
			cb(loadErrors);
			return;
		}

		// Create model objects and scene
		for (var i = 0; i < loadResults.Models.RoomModel.meshes.length; i++) {
			var mesh = loadResults.Models.RoomModel.meshes[i];
			me.meshArray = loadResults.Models.RoomModel.meshes;
			switch (mesh.name) {
				// Object table
				case 'TableMesh':
					me.TableMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0.4, 0, 1)
					);
					mat4.translate(
						me.TableMesh.world, me.TableMesh.world,
						vec3.fromValues(-1.8, -2.5, 0.49672)
					);
					me.TableMesh.currentMat = me.TableMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object sofa
				case 'SofaMesh':
					me.SofaMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.1, 0.1, 0.1, 1)
					);
					mat4.translate(
						me.SofaMesh.world, me.SofaMesh.world,
						vec3.fromValues(-5, -2.5, 0.78448)
					);
					me.SofaMesh.currentMat = me.SofaMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object lightbulb/lamp
				case 'LightBulbMesh':
					me.lightPosition = vec3.fromValues(0, 0.0, 2.58971);
					me.LightMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(4, 4, 4, 1)
					);
					mat4.translate(me.LightMesh.world, me.LightMesh.world,
						me.lightPosition
					);
					me.LightMesh.currentMat = me.LightMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object walls
				case 'WallsMesh':
					me.WallsMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.8, 0.6, 0.3, 0.8)
					);
					me.WallsMesh.currentMat = me.WallsMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object floor
				case 'WallFloorMesh':
					me.WallFloorMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.4, 0.2, 0.1, 1)
					);
					mat4.translate(
						me.WallFloorMesh.world, me.WallFloorMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.WallFloorMesh.world, me.WallFloorMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.WallFloorMesh.currentMat = me.WallFloorMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object dark blue carpet
				case 'CarpetMesh':
					me.CarpetMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.2, 0.2, 0.7, 1)
					);
					mat4.translate(
						me.CarpetMesh.world, me.CarpetMesh.world,
						vec3.fromValues(1, 0, 0)
					);
					mat4.rotate(
						me.CarpetMesh.world, me.CarpetMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CarpetMesh.currentMat = me.CarpetMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object light blue carpet
				case 'Carpet2Mesh':
					me.Carpet2Mesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0, 0.7, 0.7, 1)
					);
					mat4.translate(
						me.Carpet2Mesh.world, me.Carpet2Mesh.world,
						vec3.fromValues(6, 5, 0)
					);
					mat4.rotate(
						me.Carpet2Mesh.world, me.Carpet2Mesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.Carpet2Mesh.currentMat = me.Carpet2Mesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object bedframe
				case 'BedFrameMesh':
					me.BedFrameMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.8, 0.8, 0.8, 1)
					);
					mat4.translate(
						me.BedFrameMesh.world, me.BedFrameMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.BedFrameMesh.world, me.BedFrameMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.BedFrameMesh.currentMat = me.BedFrameMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object bedsheet
				case 'BedSheetMesh':
					me.BedSheetMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.3, 0.2, 0.7, 0.8)
					);
					mat4.translate(
						me.BedSheetMesh.world, me.BedSheetMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.BedSheetMesh.world, me.BedSheetMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.BedSheetMesh.currentMat = me.BedSheetMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object bin
				case 'BinMesh':
					me.BinMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0.5, 0.2, 1)
					);
					mat4.translate(
						me.BinMesh.world, me.BinMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.BinMesh.world, me.BinMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.BinMesh.currentMat = me.BinMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object door
				case 'DoorMesh':
					me.DoorMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0.4, 0, 1)
					);
					mat4.translate(
						me.DoorMesh.world, me.DoorMesh.world,
						vec3.fromValues(-4.99562, 3.15273,0)
					);
					mat4.rotate(
						me.DoorMesh.world, me.DoorMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.DoorMesh.currentMat = mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object door knob
				case 'DoorKnobMesh':
					me.DoorKnobMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.8, 0.8, 0.8, 1)
					);
					mat4.translate(
						me.DoorKnobMesh.world, me.DoorKnobMesh.world,
						vec3.fromValues(-4.7718, 2.6453, -1.61719)
					);
					mat4.rotate(
						me.DoorKnobMesh.world, me.DoorKnobMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.DoorKnobMesh.currentMat =mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object frame
				case 'FrameMesh':
					me.FrameMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.3, 0.8, 0.2, 0.95)
					);
					mat4.translate(
						me.FrameMesh.world, me.FrameMesh.world,
						vec3.fromValues(0, 0.2, 0)
					);
					mat4.rotate(
						me.FrameMesh.world, me.FrameMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.FrameMesh.currentMat = me.FrameMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object bedside drawer/table
				case 'DrawerMesh':
					me.DrawerMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0.4, 0, 1)
					);
					mat4.translate(
						me.DrawerMesh.world, me.DrawerMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.DrawerMesh.world, me.DrawerMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.DrawerMesh.currentMat = me.DrawerMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object book on table
				case 'TableBookMesh':
					me.TableBookMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0, 0, 1, 1)
					);
					mat4.translate(
						me.TableBookMesh.world, me.TableBookMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.TableBookMesh.world, me.TableBookMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.TableBookMesh.currentMat = me.TableBookMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object cup on table
				case 'TableCupMesh':
					me.TableCupMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.8, 0.8, 0.8, 1)
					);
					mat4.translate(
						me.TableCupMesh.world, me.TableCupMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.TableCupMesh.world, me.TableCupMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.TableCupMesh.currentMat = me.TableCupMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object table legs
				case 'TableLegsMesh':
					me.TableLegsMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.1, 0.1, 0.1, 1)
					);
					mat4.translate(
						me.TableLegsMesh.world, me.TableLegsMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.TableLegsMesh.world, me.TableLegsMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.TableLegsMesh.currentMat = me.TableLegsMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object table top
				case 'TableTopMesh':
					me.TableTopMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0.4, 0, 1)
					);
					mat4.translate(
						me.TableTopMesh.world, me.TableTopMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.TableTopMesh.world, me.TableTopMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.TableTopMesh.currentMat = me.TableTopMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object ball on shelf
				case 'ShelfBallMesh':
					me.ShelfBallMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.7, 0, 0.5, 0.8)
					);
					mat4.translate(
						me.ShelfBallMesh.world, me.ShelfBallMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.ShelfBallMesh.world, me.ShelfBallMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.ShelfBallMesh.currentMat = mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object shelf
				case 'ShelfMesh':
					me.ShelfMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.8, 0.8, 0.8, 1)
					);
					mat4.translate(
						me.ShelfMesh.world, me.ShelfMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.ShelfMesh.world, me.ShelfMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.ShelfMesh.currentMat = mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object chair seat
				case 'ChairMesh':
					me.ChairMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.1, 0.1, 0.1, 1)
					);
					mat4.translate(
						me.ChairMesh.world, me.ChairMesh.world,
						vec3.fromValues(-2.4, 0, 2)
					);
					me.ChairMesh.currentMat = mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object chair stand
				case 'ChairStandMesh':
					me.ChairStandMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.5, 0.5, 0.5, 1)
					);
					mat4.translate(
						me.ChairStandMesh.world, me.ChairStandMesh.world,
						vec3.fromValues(-2.7, -2, 0)
					);
					mat4.rotate(
						me.ChairStandMesh.world, me.ChairStandMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.ChairStandMesh.currentMat = mat4.create();
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object computer's base
				case 'CompBaseMesh':
					me.CompBaseMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.5, 0.5, 0.5, 1)
					);
					mat4.translate(
						me.CompBaseMesh.world, me.CompBaseMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.CompBaseMesh.world, me.CompBaseMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CompBaseMesh.currentMat = me.CompBaseMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object computer's keyboard
				case 'CompBoardMesh':
					me.CompBoardMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.3, 0.3, 0.3, 1)
					);
					mat4.translate(
						me.CompBoardMesh.world, me.CompBoardMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.CompBoardMesh.world, me.CompBoardMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CompBoardMesh.currentMat = me.CompBoardMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object computer's plane
				case 'CompPlaneMesh':
					me.CompPlaneMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.1, 0.1, 0.1, 0.9)
					);
					mat4.translate(
						me.CompPlaneMesh.world, me.CompPlaneMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.CompPlaneMesh.world, me.CompPlaneMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CompPlaneMesh.currentMat = me.CompPlaneMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object computer's screen
				case 'CompScreenMesh':
					me.CompScreenMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.5, 0.5, 0.5, 1)
					);
					mat4.translate(
						me.CompScreenMesh.world, me.CompScreenMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.CompScreenMesh.world, me.CompScreenMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CompScreenMesh.currentMat = me.CompScreenMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
				// Object computer's stand
				case 'CompStandMesh':
					me.CompStandMesh = new Model(
						me.gl, mesh.vertices, [].concat.apply([], mesh.faces),
						mesh.normals, vec4.fromValues(0.5, 0.5, 0.5, 1)
					);
					mat4.translate(
						me.CompStandMesh.world, me.CompStandMesh.world,
						vec3.fromValues(0, 0, 0)
					);
					mat4.rotate(
						me.CompStandMesh.world, me.CompStandMesh.world,
						glMatrix.toRadian(90),
						vec3.fromValues(1, 0, 0)
					);
					me.CompStandMesh.currentMat = me.CompStandMesh.world;
					me.meshArray = loadResults.Models.RoomModel.meshes;
					break;
			}
		}

		if (!me.TableMesh) {
			cb('Failed to load table mesh'); return;
		}
		if (!me.SofaMesh) {
			cb('Failed to load sofa mesh'); return;
		}
		if (!me.LightMesh) {
			cb('Failed to load light mesh'); return;
		}
		if (!me.WallsMesh) {
			cb('Failed to load walls mesh'); return;
		}
		if (!me.WallFloorMesh) {
			cb('Failed to load wallsfloor mesh'); return;
		}
		if (!me.CarpetMesh) {
			cb('Failed to load carpet mesh'); return;
		}
		if (!me.Carpet2Mesh) {
			cb('Failed to load carpet2 mesh'); return;
		}
		if (!me.BedFrameMesh) {
			cb('Failed to load bedframe mesh'); return;
		}
		if (!me.BedSheetMesh) {
			cb('Failed to load bedsheet mesh'); return;
		}
		if (!me.BinMesh) {
			cb('Failed to load bin mesh'); return;
		}
		if (!me.DoorMesh) {
			cb('Failed to load door mesh'); return;
		}
		if (!me.DoorKnobMesh) {
			cb('Failed to load doorknob mesh'); return;
		}
		if (!me.FrameMesh) {
			cb('Failed to load frame mesh'); return;
		}
		if (!me.DrawerMesh) {
			cb('Failed to load drawer mesh'); return;
		}
		if (!me.TableBookMesh) {
			cb('Failed to load tablebook mesh'); return;
		}
		if (!me.TableCupMesh) {
			cb('Failed to load tablecup mesh'); return;
		}
		if (!me.TableLegsMesh) {
			cb('Failed to load tablelegs mesh'); return;
		}
		if (!me.TableTopMesh) {
			cb('Failed to load tabletop mesh'); return;
		}
		if (!me.ShelfBallMesh) {
			cb('Failed to load shelfball mesh'); return;
		}
		if (!me.ShelfMesh) {
			cb('Failed to load shelf mesh'); return;
		}
		if (!me.ChairMesh) {
			cb('Failed to load chair mesh'); return;
		}
		if (!me.ChairStandMesh) {
			cb('Failed to load chairstand mesh'); return;
		}
		if (!me.CompBaseMesh) {
			cb('Failed to load compbase mesh'); return;
		}
		if (!me.CompBoardMesh) {
			cb('Failed to load compboard mesh'); return;
		}
		if (!me.CompPlaneMesh) {
			cb('Failed to load compplane mesh'); return;
		}
		if (!me.CompScreenMesh) {
			cb('Failed to load compscreen mesh'); return;
		}
		if (!me.CompStandMesh) {
			cb('Failed to load compstand mesh'); return;
		}
		me.Meshes = [
			me.TableMesh,
			me.SofaMesh,
			me.LightMesh,
			me.WallsMesh,
			me.WallFloorMesh,
			me.CarpetMesh,
			me.Carpet2Mesh,
			me.BedFrameMesh,
			me.BedSheetMesh,
			me.BinMesh,
			me.DoorMesh,
			me.DoorKnobMesh,
			me.FrameMesh,
			me.DrawerMesh,
			me.TableBookMesh,
			me.TableCupMesh,
			me.TableLegsMesh,
			me.TableTopMesh,
			me.ShelfBallMesh,
			me.ShelfMesh,
			me.ChairMesh,
			me.ChairStandMesh,
			me.CompBaseMesh,
			me.CompBoardMesh,
			me.CompPlaneMesh,
			me.CompScreenMesh,
			me.CompStandMesh
		];

		// Create shaders
		me.ShadowProgram = CreateShaderProgram(
			me.gl, loadResults.ShaderCode.Shadow_VSText,
			loadResults.ShaderCode.Shadow_FSText
		);
		if (me.ShadowProgram.error) {
			cb('ShadowProgram ' + me.ShadowProgram.error); return;
		}

		me.ShadowMapGenProgram = CreateShaderProgram(
			me.gl, loadResults.ShaderCode.ShadowMapGen_VSText,
			loadResults.ShaderCode.ShadowMapGen_FSText
		);
		if (me.ShadowMapGenProgram.error) {
			cb('ShadowMapGenProgram ' + me.ShadowMapGenProgram.error); return;
		}

		me.ShadowProgram.uniforms = {
			mProj: me.gl.getUniformLocation(me.ShadowProgram, 'mProj'),
			mView: me.gl.getUniformLocation(me.ShadowProgram, 'mView'),
			mWorld: me.gl.getUniformLocation(me.ShadowProgram, 'mWorld'),

			pointLightPosition: me.gl.getUniformLocation(me.ShadowProgram, 'pointLightPosition'),
			meshColor: me.gl.getUniformLocation(me.ShadowProgram, 'meshColor'),
			sampler: me.gl.getUniformLocation(me.ShadowProgram, 'sampler'),
			lightShadowMap: me.gl.getUniformLocation(me.ShadowProgram, 'lightShadowMap'),
			shadowClipNearFar: me.gl.getUniformLocation(me.ShadowProgram, 'shadowClipNearFar'),

			bias: me.gl.getUniformLocation(me.ShadowProgram, 'bias')
		};
		me.ShadowProgram.attribs = {
			vPos: me.gl.getAttribLocation(me.ShadowProgram, 'vPos'),
			vNorm: me.gl.getAttribLocation(me.ShadowProgram, 'vNorm'),
			vertTexCoord: me.gl.getAttribLocation(me.ShadowProgram, 'verTexCoord'),
		};

		me.ShadowMapGenProgram.uniforms = {
			mProj: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mProj'),
			mView: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mView'),
			mWorld: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'mWorld'),

			pointLightPosition: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'pointLightPosition'),
			shadowClipNearFar: me.gl.getUniformLocation(me.ShadowMapGenProgram, 'shadowClipNearFar'),
		};
		me.ShadowMapGenProgram.attribs = {
			vPos: me.gl.getAttribLocation(me.ShadowMapGenProgram, 'vPos'),
		};

		// Create framebuffers and textures
		me.shadowMapCube = me.gl.createTexture();
		me.gl.bindTexture(me.gl.TEXTURE_CUBE_MAP, me.shadowMapCube);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_MIN_FILTER, me.gl.LINEAR);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_MAG_FILTER, me.gl.LINEAR);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_WRAP_S, me.gl.MIRRORED_REPEAT);
		me.gl.texParameteri(me.gl.TEXTURE_CUBE_MAP, me.gl.TEXTURE_WRAP_T, me.gl.MIRRORED_REPEAT);
		me.floatExtension = me.gl.getExtension("OES_texture_float");
		me.floatLinearExtension = me.gl.getExtension("OES_texture_float_linear");
		if (me.floatExtension && me.floatLinearExtension) {
			for (var i = 0; i < 6; i++) {
				me.gl.texImage2D(
					me.gl.TEXTURE_CUBE_MAP_POSITIVE_X + i,
					0, me.gl.RGBA,
					me.textureSize, me.textureSize,
					0, me.gl.RGBA,
					me.gl.FLOAT, null
				);
			}
		} else {
			for (var i = 0; i < 6; i++) {
				me.gl.texImage2D(
					me.gl.TEXTURE_CUBE_MAP_POSITIVE_X + i,
					0, me.gl.RGBA,
					me.textureSize, me.textureSize,
					0, me.gl.RGBA,
					me.gl.UNSIGNED_BYTE, null
				);
			}
		}

		me.shadowMapFramebuffer = me.gl.createFramebuffer();
		me.gl.bindFramebuffer(me.gl.FRAMEBUFFER, me.shadowMapFramebuffer);

		me.shadowMapRenderbuffer = me.gl.createRenderbuffer();
		me.gl.bindRenderbuffer(me.gl.RENDERBUFFER, me.shadowMapRenderbuffer);
		me.gl.renderbufferStorage(
			me.gl.RENDERBUFFER, me.gl.DEPTH_COMPONENT16,
			me.textureSize, me.textureSize
		);

		me.gl.bindTexture(me.gl.TEXTURE_CUBE_MAP, null);
		me.gl.bindRenderbuffer(me.gl.RENDERBUFFER, null);
		me.gl.bindFramebuffer(me.gl.FRAMEBUFFER, null);

		me.cameraIndex = 0
		me.mainCamera = new Camera(
			vec3.fromValues(4.5, -4.5, 1.85),
			vec3.fromValues(0, 1.5, 1.85),
			vec3.fromValues(0, 0, 1)
		);

		me.chairCamera = new Camera(
			vec3.fromValues(0, 0, 1.85),
			vec3.fromValues(7, 0, 1.85),
			vec3.fromValues(0, 0, 1)
		);
		me.chairCamera.position = vec3.fromValues(2, 2, 2)
		me.chairCamera.rotateRight(-Math.PI/2)  // TODO Ugly rotation correction

		me.projMatrix = mat4.create();
		me.viewMatrix = mat4.create();

		mat4.perspective(
			me.projMatrix,
			glMatrix.toRadian(90),
			me.gl.canvas.clientWidth / me.gl.canvas.clientHeight,
			0.35,
			85.0
		);

		me.shadowMapCameras = [
			// Positive X
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(1, 0, 0)),
				vec3.fromValues(0, -1, 0)
			),
			// Negative X
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(-1, 0, 0)),
				vec3.fromValues(0, -1, 0)
			),
			// Positive Y
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 1, 0)),
				vec3.fromValues(0, 0, 1)
			),
			// Negative Y
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, -1, 0)),
				vec3.fromValues(0, 0, -1)
			),
			// Positive Z
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 0, 1)),
				vec3.fromValues(0, -1, 0)
			),
			// Negative Z
			new Camera(
				me.lightPosition,
				vec3.add(vec3.create(), me.lightPosition, vec3.fromValues(0, 0, -1)),
				vec3.fromValues(0, -1, 0)
			),
		];
		me.shadowMapViewMatrices = [
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create(),
			mat4.create()
		];
		me.shadowMapProj = mat4.create();
		me.shadowClipNearFar = vec2.fromValues(0.05, 15.0);
		mat4.perspective(
			me.shadowMapProj,
			glMatrix.toRadian(90),
			1.0,
			me.shadowClipNearFar[0],
			me.shadowClipNearFar[1]
		);

		cb();
	});

	me.PressedKeys = {
		Up: false,
		Right: false,
		Down: false,
		Left: false,
		Forward: false,
		Back: false,

		RotLeft: false,
		RotRight: false,
	};

	me.MoveForwardSpeed = 3.5;
	me.RotateSpeed = 1.5;
	me.textureSize = getParameterByName('texSize') || 512;

	me.lightDisplacementInputAngle = 0.0;
};

BedroomScene.prototype.Unload = function () {
	this.LightMesh = null;
	this.TableMesh = null;
	this.SofaMesh = null;
	this.WallsMesh = null;
	this.WallFloorMesh = null;
	this.CarpetMesh = null;
	this.Carpet2Mesh = null;
	this.BedFrameMesh = null;
	this.BedSheetMesh = null;
	this.BinMesh = null;
	this.DoorMesh = null;
	this.DoorKnobMesh = null;
	this.FrameMesh = null;
	this.DrawerMesh = null;
	this.TableBookMesh = null;
	this.TableCupMesh = null;
	this.TableLegsMesh = null;
	this.TableTopMesh = null;
	this.ShelfBallMesh = null;
	this.ShelfMesh = null;
	this.ChairMesh = null;
	this.ChairStandMesh = null;
	this.CompBaseMesh = null;
	this.CompBoardMesh = null;
	this.CompPlaneMesh = null;
	this.CompScreenMesh = null;
	this.CompStandMesh = null;

	this.ShadowProgram = null;
	this.ShadowMapGenProgram = null;

	this.cameraIndex = null
	this.mainCamera = null
	this.chairCamera = null
	this.lightPosition = null;

	this.Meshes = null;

	this.PressedKeys = null;

	this.MoveForwardSpeed = null;
	this.RotateSpeed = null;

	this.shadowMapCube = null;
	this.textureSize = null;

	this.shadowMapCameras = null;
	this.shadowMapViewMatrices = null;

	this.meshArray = [];
};

BedroomScene.prototype.Begin = function () {
	console.log('Beginning bedroom scene');

	var me = this;

	// Attach event listeners
	this.__ResizeWindowListener = this._OnResizeWindow.bind(this);
	this.__KeyDownWindowListener = this._OnKeyDown.bind(this);
	this.__KeyUpWindowListener = this._OnKeyUp.bind(this);

	AddEvent(window, 'resize', this.__ResizeWindowListener);
	AddEvent(window, 'keydown', this.__KeyDownWindowListener);
	AddEvent(window, 'keyup', this.__KeyUpWindowListener);

	// Floor texture
	this.floorTexture = [];
	var newTex = me.gl.createTexture();
	me.gl.bindTexture(me.gl.TEXTURE_2D, newTex);
	me.gl.texImage2D(me.gl.TEXTURE_2D, 0, me.gl.RGBA, 1, 1, 1, me.gl.RGBA,
		me.gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
	me.gl.texParameteri(me.gl.TEXTURE_2D, me.gl.TEXTURE_WRAP_S, me.gl.CLAMP_TO_EDGE);
	me.gl.texParameteri(me.gl.TEXTURE_2D, me.gl.TEXTURE_WRAP_T, me.gl.CLAMP_TO_EDGE);
	me.gl.texParameteri(me.gl.TEXTURE_2D, me.gl.TEXTURE_MIN_FILTER, me.gl.LINEAR);
	me.gl.texParameteri(me.gl.TEXTURE_2D, me.gl.TEXTURE_MAG_FILTER, me.gl.LINEAR);
	me.gl.bindTexture(me.gl.TEXTURE_2D, null);

	var texImage = new Image();
	texImage.onload = function () {
		me.gl.uniform1i(me.ShadowProgram.uniforms.sampler, 1);
		me.gl.activeTexture(me.gl.TEXTURE1);
		me.gl.bindTexture(me.gl.TEXTURE_2D, newTex);
		me.gl.texImage2D(
			me.gl.TEXTURE_2D, 0, me.gl.RGBA, me.gl.RGBA,
			me.gl.UNSIGNED_BYTE, texImage);
		me.gl.generateMipmap(me.gl.TEXTURE_2D);
	}
	texImage.src = "textures/wood.png";
	this.floorTexture.push(newTex);

	// Render loop
	var previousFrame = performance.now();
	var dt = 0;
	var loop = function (currentFrameTime) {
		dt = currentFrameTime - previousFrame;
		me._Update(dt, currentFrameTime);

		previousFrame = currentFrameTime;

		me._GenerateShadowMap();
		me._Render();
		me.nextFrameHandle = requestAnimationFrame(loop);
	};
	me.nextFrameHandle = requestAnimationFrame(loop);

	me._OnResizeWindow();
};

BedroomScene.prototype.End = function () {
	if (this.__ResizeWindowListener) {
		RemoveEvent(window, 'resize', this.__ResizeWindowListener);
	}
	if (this.__KeyUpWindowListener) {
		RemoveEvent(window, 'keyup', this.__KeyUpWindowListener);
	}
	if (this.__KeyDownWindowListener) {
		RemoveEvent(window, 'keydown', this.__KeyDownWindowListener);
	}

	if (this.nextFrameHandle) {
		cancelAnimationFrame(this.nextFrameHandle);
	}
};

var zTranslation = -2.1;
var flag = true;
var yTranslationDoor = -3.65;
var flagDoor = true;
var yTranslationShelf = 1;
var flagShelf= true;
var direction = -1;
var angle = -90;
var deg = 0;
// Private methods
BedroomScene.prototype._Update = function (dt, currentFrameTime) {
	for (var i = 0; i < this.meshArray.length; i++) {
		var mesh = this.meshArray[i];
		switch (mesh.name) {
			// Object table
			case 'TableMesh':
				break;
			// Object sofa
			case 'SofaMesh':
				break;
			// Object lightbulb/lamp
			case 'LightBulbMesh':
				break;
			// Object walls
			case 'WallsMesh':
				break;
			// Object floor
			case 'WallFloorMesh':
				break;
			// Object dark blue carpet
			case 'CarpetMesh':
				break;
			// Object light blue carpet
			case 'Carpet2Mesh':
				break;
			// Object bedframe
			case 'BedFrameMesh':
				break;
			// Object bedsheet
			case 'BedSheetMesh':
				break;
			// Object bin
			case 'BinMesh':
				break;
			// Object door
			case 'DoorMesh':
				var matframe_wbaseDoor = mat4.create();
				mat4.translate(
					matframe_wbaseDoor, mat4.create(),
					vec3.fromValues(5, yTranslationDoor, 0)
				);
				if (!this.demoFlag) {
					mat4.translate(
						matframe_wbaseDoor, mat4.create(),
						vec3.fromValues(5, this.doorTranslate, 0)
						);
				} else {
				if(flagDoor){
					if (yTranslationDoor < -3){
						yTranslationDoor += 0.05;
					} else {;
						flagDoor = false;
					}
				} else if (!flagDoor){
					if (yTranslationDoor > -4.3){
						yTranslationDoor -= 0.05;
					} else {
						flagDoor = true;
					}
				}
				}
				mat4.multiply(this.DoorMesh.currentMat, matframe_wbaseDoor, this.DoorMesh.world);
				break;
			// Object door knob
			case 'DoorKnobMesh':
				var tempDoorKnobMat = mat4.create();
				var matframe_wbaseDoorKnob = mat4.create();
				mat4.translate(
					tempDoorKnobMat, tempDoorKnobMat,
					vec3.fromValues(4.7718, 1.6453, 2.5)
				);
				mat4.rotate(
					tempDoorKnobMat, tempDoorKnobMat,
					glMatrix.toRadian(90),
					vec3.fromValues(0, 0, 1)
				);
				if (!this.demoFlag) {
					mat4.rotateY(
						tempDoorKnobMat, tempDoorKnobMat,
						this.angleKnob * Math.PI / 180
					);
				} else {
					if (direction === -1){
						if (deg < -180){
							direction = 1;
						}
					} else {
						if (deg >= -90){
							direction = -1;
						}
					}
					angle += direction / 100;
					deg = (angle * (180 / Math.PI)) % 360;
					mat4.rotateY(
						tempDoorKnobMat, tempDoorKnobMat, angle
					);
				}
				mat4.multiply(matframe_wbaseDoorKnob, tempDoorKnobMat, this.DoorKnobMesh.world);
				mat4.multiply(this.DoorKnobMesh.currentMat, this.DoorMesh.currentMat, matframe_wbaseDoorKnob);
				break;
			// Object frame
			case 'FrameMesh':
				break;
			// Object bedside drawer/table
			case 'DrawerMesh':
				break;
			// Object book on table
			case 'TableBookMesh':
				break;
			// Object cup on table
			case 'TableCupMesh':
				break;
			// Object table legs
			case 'TableLegsMesh':
				break;
			// Object table top
			case 'TableTopMesh':
				break;
			// Object ball on shelf
			case 'ShelfBallMesh':
				var tempShelfBallMat = mat4.create();
				mat4.translate(
					tempShelfBallMat, mat4.create(),
					vec3.fromValues(0, 3.3,zTranslation)
				);

				if (!this.demoFlag) {
					mat4.translate(
						tempShelfBallMat, mat4.create(),
						vec3.fromValues(0, 3.3, this.ballTranslate)
						);
				} else {
					if(flag){
						if (zTranslation < -1.5){
							zTranslation += 0.05;
						} else {;
							flag = false;
						}
					} else if (!flag){
						if (zTranslation > -2.7){
							zTranslation -= 0.05;
						} else {
							flag = true;
						}
					}
					mat4.translate(
						tempShelfBallMat, mat4.create(),
						vec3.fromValues(0, 3.3, zTranslation)
						);
					}
				var matframe_wbaseShelfBall = mat4.create();
				mat4.multiply(matframe_wbaseShelfBall, tempShelfBallMat, this.ShelfBallMesh.world);
				mat4.multiply(this.ShelfBallMesh.currentMat, this.ShelfMesh.currentMat, matframe_wbaseShelfBall);
				break;
			// Object shelf
			case 'ShelfMesh':
				var matframe_wbaseShelf = mat4.create();
				mat4.translate(
					matframe_wbaseShelf, mat4.create(),
					vec3.fromValues(0, yTranslationShelf, 0)
				);
				if (!this.demoFlag) {
					mat4.translate(
						matframe_wbaseShelf, mat4.create(),
						vec3.fromValues(0,  this.shelfTranslate,0)
						);
				} else {
					if(flagShelf){
						if (yTranslationShelf < 2){
							yTranslationShelf += 0.05;
						} else {;
							flagShelf = false;
						}
					} else if (!flagShelf){
						if (yTranslationShelf > -0.5){
							yTranslationShelf -= 0.05;
						} else {
							flagShelf = true;
						}
					}
					}

				mat4.multiply(this.ShelfMesh.currentMat, matframe_wbaseShelf, this.ShelfMesh.world);
				break;
			// Object chair seat
			case 'ChairMesh':
				var tempChairMat = mat4.create();
				mat4.translate(
					tempChairMat, mat4.create(),
					vec3.fromValues(2.45, 0, -2.2)
				);
				var matframe_wbaseChair = mat4.create();
				mat4.multiply(matframe_wbaseChair, tempChairMat, this.ChairMesh.world);
				mat4.multiply(this.ChairMesh.currentMat, this.ChairStandMesh.currentMat, matframe_wbaseChair);
				break;
			// Object chair stand
			case 'ChairStandMesh':
				var matframe_wbaseChairStand = mat4.create();
				mat4.translate(
					matframe_wbaseChairStand, mat4.create(),
					vec3.fromValues(2.7, 2, 0)
				);
				if (this.demoFlag) {
					mat4.rotateZ(
						matframe_wbaseChairStand, matframe_wbaseChairStand,
						currentFrameTime / 1000 * 2 * Math.PI * 0.2
					);
				} else {
					mat4.rotateZ(
						matframe_wbaseChairStand, matframe_wbaseChairStand,
						this.chairAngle * Math.PI / 180
					);
				}
				mat4.multiply(this.ChairStandMesh.currentMat, matframe_wbaseChairStand, this.ChairStandMesh.world);
				break;
			// Object computer's base
			case 'CompBaseMesh':
				break;
			// Object computer's keyboard
			case 'CompBoardMesh':
				break;
			// Object computer's plane
			case 'CompPlaneMesh':
			// Object computer's screen
			case 'CompScreenMesh':
				break;
			// Object computer's stand
			case 'CompStandMesh':
				break;
		}
	}

	if (this.demoFlag) {
		this.chairCamera.rotateRight(dt / 1000 * 2 * Math.PI * 0.2)  // TODO follow up sync rotation to chairCamera
	}

	if (this.PressedKeys.Forward && !this.PressedKeys.Back) {
		this.mainCamera.moveForward(dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.Back && !this.PressedKeys.Forward) {
		this.mainCamera.moveForward(-dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.Right && !this.PressedKeys.Left) {
		this.mainCamera.moveRight(dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.Left && !this.PressedKeys.Right) {
		this.mainCamera.moveRight(-dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.Up && !this.PressedKeys.Down) {
		this.mainCamera.moveUp(dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.Down && !this.PressedKeys.Up) {
		this.mainCamera.moveUp(-dt / 1000 * this.MoveForwardSpeed);
	}

	if (this.PressedKeys.RotRight && !this.PressedKeys.RotLeft) {
		this.mainCamera.rotateRight(-dt / 1000 * this.RotateSpeed);
	}

	if (this.PressedKeys.RotLeft && !this.PressedKeys.RotRight) {
		this.mainCamera.rotateRight(dt / 1000 * this.RotateSpeed);
	}

	if (this.demoFlag) {
		this.lightDisplacementInputAngle += dt / 2337;
	}
	var xDisplacement = Math.sin(this.lightDisplacementInputAngle) * 2.8;

	this.LightMesh.world[12] = xDisplacement;
	for (var i = 0; i < this.shadowMapCameras.length; i++) {
		mat4.getTranslation(this.shadowMapCameras[i].position, this.LightMesh.world);
		this.shadowMapCameras[i].GetViewMatrix(this.shadowMapViewMatrices[i]);
	}

	switch(this.cameraIndex) {
	 	case 0:
	 		this.mainCamera.GetViewMatrix(this.viewMatrix)
	 		break
		case 1:
			this.chairCamera.GetViewMatrix(this.viewMatrix)
			break
	}
};

BedroomScene.prototype._GenerateShadowMap = function () {
	var gl = this.gl;

	// Set GL state status
	gl.useProgram(this.ShadowMapGenProgram);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.shadowMapCube);
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.shadowMapFramebuffer);
	gl.bindRenderbuffer(gl.RENDERBUFFER, this.shadowMapRenderbuffer);

	gl.viewport(0, 0, this.textureSize, this.textureSize);
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);

	// Set per-frame uniforms
	gl.uniform2fv(
		this.ShadowMapGenProgram.uniforms.shadowClipNearFar,
		this.shadowClipNearFar
	);
	gl.uniform3fv(
		this.ShadowMapGenProgram.uniforms.pointLightPosition,
		this.lightPosition
	);
	gl.uniformMatrix4fv(
		this.ShadowMapGenProgram.uniforms.mProj,
		gl.FALSE,
		this.shadowMapProj
	);

	for (var i = 0; i < this.shadowMapCameras.length; i++) {
		// Set per light uniforms
		gl.uniformMatrix4fv(
			this.ShadowMapGenProgram.uniforms.mView,
			gl.FALSE,
			this.shadowMapCameras[i].GetViewMatrix(this.shadowMapViewMatrices[i])
		);

		// Set framebuffer destination
		gl.framebufferTexture2D(
			gl.FRAMEBUFFER,
			gl.COLOR_ATTACHMENT0,
			gl.TEXTURE_CUBE_MAP_POSITIVE_X + i,
			this.shadowMapCube,
			0
		);
		gl.framebufferRenderbuffer(
			gl.FRAMEBUFFER,
			gl.DEPTH_ATTACHMENT,
			gl.RENDERBUFFER,
			this.shadowMapRenderbuffer
		);

		gl.clearColor(0, 0, 0, 1);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		// Draw meshes
		for (var j = 0; j < this.Meshes.length; j++) {
			// Per object uniforms
			gl.uniformMatrix4fv(
				this.ShadowMapGenProgram.uniforms.mWorld,
				gl.FALSE,
				this.Meshes[j].currentMat
			);

			// Set attributes
			gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[j].vbo);
			gl.vertexAttribPointer(
				this.ShadowMapGenProgram.attribs.vPos,
				3, gl.FLOAT, gl.FALSE,
				0, 0
			);
			gl.enableVertexAttribArray(this.ShadowMapGenProgram.attribs.vPos);

			gl.bindBuffer(gl.ARRAY_BUFFER, null);

			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.Meshes[j].ibo);
			gl.drawElements(gl.TRIANGLES, this.Meshes[j].nPoints, gl.UNSIGNED_SHORT, 0);
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
		}
	}

	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
};

BedroomScene.prototype._Render = function () {
	var gl = this.gl;

	// Clear back buffer, set per-frame uniforms
	gl.enable(gl.CULL_FACE);
	gl.enable(gl.DEPTH_TEST);

	gl.viewport(0, 0, gl.canvas.clientWidth, gl.canvas.clientHeight);

	gl.clearColor(0, 0, 0, 1);
	gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);

	gl.useProgram(this.ShadowProgram);
	gl.uniformMatrix4fv(this.ShadowProgram.uniforms.mProj, gl.FALSE, this.projMatrix);
	gl.uniformMatrix4fv(this.ShadowProgram.uniforms.mView, gl.FALSE, this.viewMatrix);
	gl.uniform3fv(this.ShadowProgram.uniforms.pointLightPosition, this.lightPosition);
	gl.uniform2fv(this.ShadowProgram.uniforms.shadowClipNearFar, this.shadowClipNearFar);
	if (this.floatExtension && this.floatLinearExtension) {
		gl.uniform1f(this.ShadowProgram.uniforms.bias, 0.0001);
	} else {
		gl.uniform1f(this.ShadowProgram.uniforms.bias, 0.003);
	}
	gl.uniform1i(this.ShadowProgram.uniforms.lightShadowMap, 0);
	gl.activeTexture(gl.TEXTURE0);

	if (this.lightOn) {
		gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.shadowMapCube);
	}

	// Draw meshes
	for (var i = 0; i < this.Meshes.length; i++) {
		// Per object uniforms
		gl.uniformMatrix4fv(
			this.ShadowProgram.uniforms.mWorld,
			gl.FALSE,
			this.Meshes[i].currentMat
		);
		gl.uniform4fv(
			this.ShadowProgram.uniforms.meshColor,
			this.Meshes[i].color
		);

		// Set attributes
		// Vertices
		gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].vbo);
		gl.vertexAttribPointer(
			this.ShadowProgram.attribs.vPos,
			3, gl.FLOAT, gl.FALSE,
			0, 0
		);
		gl.enableVertexAttribArray(this.ShadowProgram.attribs.vPos);

		if (i == 4) {
			//this.addTexture(gl, i);
		} else {
			gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].vbo);
			gl.vertexAttribPointer(
				this.ShadowProgram.attribs.vPos,
				3, gl.FLOAT, gl.FALSE,
				0, 0
			);
			gl.enableVertexAttribArray(this.ShadowProgram.attribs.vPos);
			// Disable texture load
			gl.disableVertexAttribArray(this.ShadowProgram.attribs.vertTexCoord);
		}

		// Normal
		gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].nbo);
		gl.vertexAttribPointer(
			this.ShadowProgram.attribs.vNorm,
			3, gl.FLOAT, gl.FALSE,
			0, 0
		);
		gl.enableVertexAttribArray(this.ShadowProgram.attribs.vNorm);

		// Cleanup
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		// Index
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.Meshes[i].ibo);
		if (this.objectMode == "wireframe") {
			gl.drawElements(gl.LINE_STRIP, this.Meshes[i].nPoints, gl.UNSIGNED_SHORT, 0);
		} else {
			gl.drawElements(gl.TRIANGLES, this.Meshes[i].nPoints, gl.UNSIGNED_SHORT, 0);
		}
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	}
};

// Event Listeners
BedroomScene.prototype._OnResizeWindow = function () {
	var gl = this.gl;

	var targetHeight = window.innerWidth * 9 / 16;

	if (window.innerHeight > targetHeight) {
		// Center vertically
		gl.canvas.width = window.innerWidth;
		gl.canvas.height = targetHeight;
		gl.canvas.style.left = '0px';
		gl.canvas.style.top = (window.innerHeight - targetHeight) / 2 + 'px';
	} else {
		// Center horizontally
		gl.canvas.width = window.innerHeight * 16 / 9;
		gl.canvas.height = window.innerHeight;
		gl.canvas.style.left = (window.innerWidth - (gl.canvas.width)) / 2 + 'px';
		gl.canvas.style.top = '0px';
	}

	gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
};

BedroomScene.prototype._OnKeyDown = function (e) {
	// For continuous key event
	switch(e.code) {
		case 'KeyW':
			this.PressedKeys.Forward = true;
			break;
		case 'KeyA':
			this.PressedKeys.Left = true;
			break;
		case 'KeyD':
			this.PressedKeys.Right = true;
			break;
		case 'KeyS':
			this.PressedKeys.Back = true;
			break;
		case 'ArrowUp':
			this.PressedKeys.Up = true;
			break;
		case 'ArrowDown':
			this.PressedKeys.Down = true;
			break;
		case 'ArrowRight':
			this.PressedKeys.RotRight = true;
			break;
		case 'ArrowLeft':
			this.PressedKeys.RotLeft = true;
			break;
	}

	// For non repeating key event
	if (!e.repeat) {
		switch (e.code) {
			case 'KeyC':
				this.cycleCameraAngle()
				break;
		}
	}
};

BedroomScene.prototype._OnKeyUp = function (e) {
	// For continuous key event
	switch(e.code) {
		case 'KeyW':
			this.PressedKeys.Forward = false;
			break;
		case 'KeyA':
			this.PressedKeys.Left = false;
			break;
		case 'KeyD':
			this.PressedKeys.Right = false;
			break;
		case 'KeyS':
			this.PressedKeys.Back = false;
			break;
		case 'ArrowUp':
			this.PressedKeys.Up = false;
			break;
		case 'ArrowDown':
			this.PressedKeys.Down = false;
			break;
		case 'ArrowRight':
			this.PressedKeys.RotRight = false;
			break;
		case 'ArrowLeft':
			this.PressedKeys.RotLeft = false;
			break;
	}
};

BedroomScene.prototype.cycleCameraAngle = function () {
	this.cameraIndex = (this.cameraIndex + 1) % 2;
}

BedroomScene.prototype.addTexture = function (gl, i) {
	// Vertices
	gl.bindBuffer(gl.ARRAY_BUFFER, this.Meshes[i].vbo);
	gl.vertexAttribPointer(
		this.ShadowProgram.attribs.vPos,
		3, gl.FLOAT, gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT, 0
	);
	gl.enableVertexAttribArray(this.ShadowProgram.attribs.vPos);

	// Textures
	gl.vertexAttribPointer(
		this.ShadowProgram.attribs.vertTexCoord,
		2,
		gl.FLOAT,
		gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT,
		3 * Float32Array.BYTES_PER_ELEMENT
	);
	gl.enableVertexAttribArray(this.ShadowProgram.attribs.vertTexCoord);

	// Bind Texture
	gl.uniform1i(this.ShadowProgram.uniforms.sampler, 1);
	gl.activeTexture(gl.TEXTURE1);
	gl.bindTexture(gl.TEXTURE_2D, this.floorTexture[0]);
};
